package com.pithymedical.diabeticlog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class MessageBox extends DialogFragment {
	private boolean eatenLast2 = false;

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	
        //make the dialog using the builder class
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());                
        builder.setMessage(R.string.have_you_eaten)   
        	   .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int id) {
			        	eatenLast2 = true;
			        	endActivity();
			        }
			   })        
               .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                   public void onClick(DialogInterface dialog, int id) {
                	   eatenLast2 = false;
                	   endActivity();
                   }
               });        
        
        //create the AlertDialog object and return it
        return builder.create();
    }
	
	//adds glucose entry to database and ends activity
	public void endActivity() {
		//get the activity of the fragment
		Activity activity = getActivity();
		
		//if the activity is GlucoseEntry, which it should be, add to the database
		if(activity instanceof GlucoseEntry) {
		    ((GlucoseEntry) activity).addToDB(eatenLast2);
		}
		getActivity().finish();
	}

}
