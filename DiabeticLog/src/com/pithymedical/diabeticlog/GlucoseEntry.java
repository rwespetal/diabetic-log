package com.pithymedical.diabeticlog;

import java.util.ArrayList;
import java.util.Collections;
import android.support.v4.app.DialogFragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class GlucoseEntry extends FragmentActivity {
        private double glucoseInput = 0;
        private DatabaseHandler myDatabase;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                //Remove title bar
        		requestWindowFeature(Window.FEATURE_NO_TITLE); 
                setContentView(R.layout.activity_glucose_entry);

                myDatabase = new DatabaseHandler(getApplicationContext());

                lastThreeReadings();

                listenForButtons();
        }

        private void lastThreeReadings(){
                TextView r1 = (TextView) findViewById(R.id.reading1);
                TextView r2 = (TextView) findViewById(R.id.reading2);
                TextView r3 = (TextView) findViewById(R.id.reading3);

                ArrayList<Double> readings = 
                                DataHelper.getGlucoseLevels(myDatabase.getAllEntries());
                Collections.reverse(readings);

            	SharedPreferences myPref = 
    					PreferenceManager.getDefaultSharedPreferences(this);

    			String unitPref = myPref.getString("pref_glucose_units", 
    					"0");

    			if(unitPref.equals("0")){
    				unitPref = "mg/dL";
    			}
    			else{
    				unitPref = "mmol/L";
    			}

                int count = 0;
                while(count < readings.size() && count < 3){
                        switch(count){
                        case 0:
                                r1.setText(readings.get(0).toString() + " "
                                                + unitPref);
                                break;
                        case 1:
                                r2.setText(readings.get(1).toString() + " "
                                                + unitPref);
                                break;
                        case 2:
                                r3.setText(readings.get(2).toString() + " "
                                                + unitPref);
                                break;
                        }
                        count++;
                }
                myDatabase.close(); 
        }

        public void listenForButtons (){
                Button next = (Button) findViewById(R.id.next);
                next.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View entry) {
                                try {
                                        //get user input
                                        EditText textBox = (EditText)findViewById(R.id.number);
                                        String textBoxVal = textBox.getText().toString();

                                        glucoseInput = Double.parseDouble(textBoxVal);

                                        //display dialog box
                                        displayBox();
                                }
                                catch (Exception ex) {
                                        //no value entered
                                }

                        }
                });
        }

        public void displayBox() {
                DialogFragment msgBox = new MessageBox();
                msgBox.show(getSupportFragmentManager(), "box1");     
        }

        /**
         * add to database
         * @param eaten - whether or not the person has eaten in the last 2 hours
         */
        public void addToDB(boolean eaten) {            
                Entry newEntry = new Entry(glucoseInput, eaten);
                myDatabase = new DatabaseHandler(getApplicationContext());
                myDatabase.addEntry(newEntry);
        }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.activity_glucose_entry, menu);
                return true;
        }

}