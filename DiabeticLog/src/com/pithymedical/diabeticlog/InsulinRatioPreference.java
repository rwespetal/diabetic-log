package com.pithymedical.diabeticlog;


import android.content.*;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.View;
import android.widget.*;

public class InsulinRatioPreference extends DialogPreference {
	private EditText carbs;
	private EditText insulin; 
	public InsulinRatioPreference(Context context, AttributeSet attrs) {
		super(context, attrs);

		setDialogLayoutResource(R.layout.ratio_dialog);
		setNegativeButtonText(android.R.string.cancel);
		setPositiveButtonText(android.R.string.ok);

		setDialogIcon(null);
	}
	protected void onBindDialogView(View view) {

		carbs = (EditText) view.findViewById(R.id.ratio_carbs);
		insulin = (EditText) view.findViewById(R.id.ratio_insulin);

		SharedPreferences pref = getSharedPreferences();
		String temp = pref.getString(getKey() + SettingsActivity.InsulinRatio, "");
		if(temp.indexOf("/") == -1){
			temp = "0/0";
		}
		carbs.setText(temp.substring(0, temp.indexOf("/")));
		insulin.setText(temp.substring(temp.indexOf("/") + 1));

	}
	@Override
	protected void onDialogClosed(boolean positiveResult) {
		// When the user selects "OK", persist the new value
		if (!positiveResult) {
			return;
		}
		String save = carbs.getText() + "/" + insulin.getText();
		SharedPreferences.Editor editor = getEditor();
		editor.putString(getKey() + SettingsActivity.InsulinRatio, save); 
		editor.commit();
		persistString(save);

		super.onDialogClosed(positiveResult);
	}
}