package com.pithymedical.diabeticlog;



import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class GlucoseDisclaimer extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.disclaimer);	
	}
	
	public void listenForButtons (){} // none now

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.activity_glucose_entry, menu);
		return true;
	}
}
