package com.pithymedical.diabeticlog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class InfoBox extends DialogFragment {
        
        @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        
        //make the dialog using the builder class
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());                
        builder.setMessage(R.string.set_ratio)   
                   .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                        //getActivity().finish();
                                        Intent intent = new Intent(getActivity(), SettingsActivity.class);
                                                startActivity(intent);
                                }
                           });        
        
        //create the AlertDialog object and return it
        return builder.create();
    }   

}