package com.pithymedical.diabeticlog;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidplot.Plot;
import com.androidplot.series.XYSeries;
import com.androidplot.xy.LineAndPointFormatter;
import com.androidplot.xy.PointLabelFormatter;
import com.androidplot.xy.SimpleXYSeries;
import com.androidplot.xy.XYPlot;


public class HistoryGraphFragment extends Fragment{

	public XYPlot myPlot;

	private DatabaseHandler myDatabase;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_graph, container, false);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Activity activity = getActivity();

		if (activity != null) {
			myPlot = (XYPlot) getView().findViewById(R.id.mySimpleXYPlot);
			myDatabase = new DatabaseHandler(activity.getApplicationContext());

			ArrayList<Entry> tableEntries = 
					(ArrayList<Entry>) myDatabase.getAllEntries();
			//ArrayList<Time> times = DataHelper.getTimes(tableEntries);
			ArrayList<Long> timesRaw = DataHelper.getTimesLong(tableEntries);
			ArrayList<Double> glucoseLevels = 
					DataHelper.getGlucoseLevels(tableEntries);

			SharedPreferences myPref = 
					PreferenceManager.getDefaultSharedPreferences(activity);

			String unitPref = myPref.getString("pref_glucose_units", 
					"0");

			if(unitPref.equals("0")){
				unitPref = "mg/dL";
			}
			else{
				unitPref = "mmol/L";
			}

			// Log.d("GRAPH", "Times size: " + times.size());
			// Log.d("GRAPH", "glucoseLevels size: " + glucoseLevels.size());

			//ArrayList<Double> ftimes = DataHelper.getDateDouble(times);

			XYSeries series = new SimpleXYSeries(timesRaw, glucoseLevels,
					"Glucose levels");

			// Create a formatter to use for drawing a series using LineAndPointRenderer:
			LineAndPointFormatter series1Format = new LineAndPointFormatter(
					Color.rgb(0, 200, 0),                   // line color
					Color.rgb(0, 100, 0),                   // point color
					null,                                   // fill color (none)
					new PointLabelFormatter(Color.TRANSPARENT));  // text color 

			myPlot.setDomainValueFormat(new DateTimeFormatter());
			myPlot.getGraphWidget().setMarginTop(10);
			myPlot.getGraphWidget().setMarginRight(2);
			myPlot.setBorderStyle(Plot.BorderStyle.SQUARE, null, null);
			myPlot.setRangeLabel("Glucose Level (" + unitPref + ")");
			myPlot.setDomainLabel("Date");
			myPlot.setPlotPadding(10,10,10,20);
			if(!timesRaw.isEmpty()){

				double glucoseMin = Collections.min(glucoseLevels);
				double glucoseMax = Collections.max(glucoseLevels);
				long timeMin = Collections.min(timesRaw); 
				long timeMax = Collections.max(timesRaw); 
				
				double ratio = (glucoseMax - glucoseMin) * .05;
				double timeRatio = (timeMax - timeMin) * .1; 

				if(glucoseMin - ratio < 0){
					myPlot.setRangeBottomMax(0);
				}
				else{
					myPlot.setRangeBottomMax(glucoseMin - ratio);
				}
				
				myPlot.setRangeTopMin(glucoseMax + ratio);
				myPlot.setDomainRightMin(timeMax + timeRatio);
				myPlot.setDomainLeftMax(timeMin - timeRatio);
			}
			myPlot.addSeries(series, series1Format);

		}
		myDatabase.close(); 
	}

}