package com.pithymedical.diabeticlog;

import java.text.DecimalFormat;

import android.support.v4.app.DialogFragment;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;

public class DoseBox extends DialogFragment {
	private Double doseCalculation;
	private int carbs;
	private double divisor;
	

	@Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
    	//get the fragment's activity
    	Activity activity = getActivity();

    	//grab carbs from fragment's parent activity
    	carbs = ((InsulinCalculator) activity).getCarbs();
    	
    	//get ratio of carbs to insulin
    	divisor = ((InsulinCalculator) activity).parseRatio();
    	
    	//get dose
    	doseCalculation = (double) Math.round(carbs / divisor);
    	    	
        //make the dialog using the builder class
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(new DecimalFormat("#").format(doseCalculation))
        	   .setTitle(R.string.dose_box)
        	   .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int id) {
			        	
			        	//return to main screen
			        	getActivity().finish();
			        	
			        }
			   });                
        
        //create the AlertDialog object and return it
        return builder.create();
    }
}
