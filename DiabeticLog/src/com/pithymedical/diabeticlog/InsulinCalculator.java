package com.pithymedical.diabeticlog;


import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class InsulinCalculator extends FragmentActivity {
        private int totalCarbs;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                //Remove title bar
        		requestWindowFeature(Window.FEATURE_NO_TITLE); 
                setContentView(R.layout.activity_insulin_calculator);

                listenForButtons();
                
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.insulin_calculator, menu);
                return true;
        }
        
        
        public void listenForButtons (){
                Button calculate = (Button) findViewById(R.id.button_calc_insulin_dosage);
                calculate.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View entry) {
                                
                                //check to ensure that user has set a carbs to insulin ratio
                                if (getInsulinPreference().equals("")) {
                                        DialogFragment infoBox = new InfoBox();
                                        infoBox.show(getSupportFragmentManager(), "infoBox");  
                                        return;
                                }
                                
                                //get value from total carbs EditText control
                                EditText carbsTextBox = (EditText) findViewById(R.id.txtTotalCarbs);
                                try {
                                        totalCarbs = Integer.parseInt(carbsTextBox.getText().toString());
                                }
                                catch (Exception ex) {
                                        //no value entered
                                        return;
                                }
                                                                
                                DialogFragment insulinBox = new DoseBox();
                                insulinBox.show(getSupportFragmentManager(), "insulinBox");
                        }
                });

        }
        
        private String getInsulinPreference() {
                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);             
                return sharedPref.getString(SettingsActivity.InsulinRatio, "");
        }
        
        
        public int getCarbs () {                
                return totalCarbs;
        }
        
        //gets the ratio string from shared preferences and parses it 
        public double parseRatio() {
                double ratio;
                String tempValues[] = new String[1];
                                
                String insulin_ratio = getInsulinPreference(); 
                
                //parse
                tempValues = insulin_ratio.split("/");
                        
                //e.g., 15 carbs / 1 unit of insulin
                ratio = Double.parseDouble(tempValues[0]) / Double.parseDouble(tempValues[1]);  
                
                return ratio;
        }
        

}