package com.pithymedical.diabeticlog;

import java.util.ArrayList;
import java.util.Collections;



import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class HistoryTableFragment extends Fragment {

	private DatabaseHandler myDatabase;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_table, container, false);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		Activity activity = getActivity();

		if (activity != null) {

			TableLayout myTable = (TableLayout) 
					activity.findViewById(R.id.tableLayout);

			//TableRow.LayoutParams params = new TableRow.LayoutParams();
			myDatabase = new DatabaseHandler(activity.getApplicationContext());

			ArrayList<Entry> tableEntries = (ArrayList<Entry>) myDatabase.getAllEntries();
			Collections.reverse(tableEntries);
			ArrayList<Time> times = DataHelper.getTimes(tableEntries);
			ArrayList<Double> glucoseLevels = 
					DataHelper.getGlucoseLevels(tableEntries);

			SharedPreferences myPref = 
					PreferenceManager.getDefaultSharedPreferences(activity);

			String unitPref = myPref.getString("pref_glucose_units", 
					"0");

			if(unitPref.equals("0")){
				unitPref = "mg/dL";
			}
			else{
				unitPref = "mmol/L";
			}

			for(int i = 0; i < times.size(); i++){
				TableRow newRow = new TableRow(activity);

				TextView tv1 = new TextView(activity);
				TextView tv2 = new TextView(activity);

				tv1.setText(formatTime(DatabaseHandler.timeToString(times.get(i))));
				tv2.setText(glucoseLevels.get(i).toString() + " " + unitPref);

				tv1.setGravity(Gravity.CENTER_HORIZONTAL);
				tv2.setGravity(Gravity.CENTER_HORIZONTAL);

				tv1.setTextSize(18);
				tv2.setTextSize(18);

				newRow.addView(tv1);
				newRow.addView(tv2);

				myTable.addView(newRow);
			}
		}
		myDatabase.close(); 
	}

	public static String formatTime(String time){
		String[] split = time.split(" ");

		String date = split[0];
		time = split[1];

		date = date.substring(date.indexOf('-')+1);
		date.replaceFirst("-", "/");
		String[] dateSplit = date.split("-");
		

		split = time.split(":");
		int hours = Integer.parseInt(split[0]);
		int minutes = Integer.parseInt(split[1]);
		double remainder = hours/12;
		hours = hours%12;
		if(hours == 0){
			hours = 12;
		}
		String hourString = "";
		String minuteString = "";
		if((double)minutes/10 < 1){
			minuteString = minuteString + "0" + minutes;
		}
		else{
			minuteString = minuteString + minutes;
		}
		
		hourString = hourString + hours;
		
		String am = "am";
		if(remainder >= 1){
			am = "pm";
		}
		
		return dateSplit[0] + "/" + dateSplit[1] + " \u2014 " + hourString + ":" 
			+ minuteString + am; 
	}

}
