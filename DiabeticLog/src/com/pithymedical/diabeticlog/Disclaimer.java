package com.pithymedical.diabeticlog;



import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

public class Disclaimer extends DialogPreference {
    public Disclaimer(Context context, AttributeSet attrs) {
        super(context, attrs);
        	setDialogLayoutResource(R.layout.disclaimer);
        	setPositiveButtonText(android.R.string.ok);
        	
        	//Removes the cancel button, just displays the ok
        	setNegativeButtonText("");
        	setDialogIcon(null);
    }
}
