//http://www.androidhive.info/2011/11/android-sqlite-database-tutorial/

package com.pithymedical.diabeticlog;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.util.Log;

/*  TABLE_ENTRIES
 *  ----------------------------------
 * | Time | Glucose Level | Pre Meal? |
 * |----------------------------------
 * | .... | ............. | ......... |
 * | .... | ............. | ......... |
 * | .... | ............. | ......... |
 *  ----------------------------------
 *  
 *  Time is the unique key for this table, as such, we do not allow time to be
 *  updated; only Glucose Level and Pre Meal values can be updated.
 */
 
public class DatabaseHandler extends SQLiteOpenHelper {
 
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "database";
    private static final String TABLE_ENTRIES = "entryTable";
    private static final String KEY_INDEX = "_id";
    private static final String KEY_TIME = "Time";
    private static final String KEY_GLUCOSE_LEVEL = "Glucose";
    private static final String KEY_PRE_MEAL = "Pre";
    @SuppressWarnings("unused")
	private SQLiteDatabase db; 
    private Context context; 
    
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context; 
        this.db = this.getWritableDatabase();  
    }
 
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_ENTRIES + "(" + KEY_INDEX + " INTEGER PRIMARY KEY AUTOINCREMENT," 
                + KEY_TIME + " TEXT," + KEY_GLUCOSE_LEVEL + " TEXT,"
                + KEY_PRE_MEAL + " TEXT" + ");";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRIES);
        // Create tables again
        onCreate(db);
    }
 
    /**
     * This method adds the specified Entry to the database. All Entry values
     * must be valid, else exception will be thrown when getting Entry values.
     * 
     * @param entry (Entry) - Entry to be added to database table.
     */
    void addEntry(Entry entry) {
    	// Make DB writeable, create new row object to add, insert it, close DB
    	try{
    		SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_TIME, timeToString(entry.getTime()));
            String glucoseLevel = String.valueOf(entry.getGlucoseLevel());
            values.put(KEY_GLUCOSE_LEVEL, glucoseLevel);
            values.put(KEY_PRE_MEAL, entry.getPreMeal().toString());
            db.insert(TABLE_ENTRIES, null, values);
            db.close(); // Closing database connection
    	}catch(Exception e){
    		Log.d("DB test", "fail: " + e.toString());
    		Log.d("DB test", "fail: " + e.getMessage());
    	}
    }
 
    /**
     * This method converts a Time type to a string in the ISO8601 standard.
     * "YYYY-MM-DD HH:MM:SS"
     * @param The time to convert
     * @return The string of the time converted
     */
	public static String timeToString(Time time){
    	String newTime = "";
    	newTime += time.year;
    	newTime += "-";
    	newTime += time.month + 1;
    	newTime += "-";
    	newTime += time.monthDay;
    	newTime += " ";
    	newTime += time.hour;
    	newTime += ":";
    	newTime += time.minute;
    	newTime += ":";
    	newTime += time.second;
    	return newTime;
    }
	
	/**
	 * This method converts a string in ISO8601 format to java time type.
	 * @param time - String in ISO8601
	 * @return The time of the string
	 */
	public static Time stringToTime(String time){
		String[] tokens = time.split("-|\\ |\\:");
		Time newTime = new Time();
		newTime.year = Integer.valueOf(tokens[0]);
		newTime.month = Integer.valueOf(tokens[1]) - 1;
		newTime.monthDay = Integer.valueOf(tokens[2]);
		newTime.hour = Integer.valueOf(tokens[3]);
		newTime.minute = Integer.valueOf(tokens[4]);
		newTime.second = Integer.valueOf(tokens[5]);
		return newTime;
	}
    
     /**
  	 * This method searches the data base for the first instance of the
  	 * first instance of the specified Time, returns entry at time.
  	 * 
     * @param time - (Time) Time to search DB table for
     * @return (Entry) - Entry with specified time
     */
    Entry getEntry(Time time) {
    	// create queury using time as unique key, find and return entry
        SQLiteDatabase db = this.getReadableDatabase();
        String timeToFind = timeToString(time);
       
        	Cursor cursor = db.query(TABLE_ENTRIES, new String[] { KEY_TIME,
                    KEY_GLUCOSE_LEVEL, KEY_PRE_MEAL }, KEY_TIME + "=?",
                    new String[] { timeToFind }, null, null, null, null);
        	if (cursor != null)
                cursor.moveToFirst();

        // search DB using specified query 
        Time timeTemp = stringToTime(cursor.getString(0));
        double levelTemp = Double.parseDouble(cursor.getString(1));
        Boolean preTemp = Boolean.parseBoolean(cursor.getString(2));
        Entry entry = new Entry(timeTemp, levelTemp, preTemp);
        return entry;
    }
    
    /**
     * This method queries the DB for an entry at the specified index
     * @param index - The index for the entry to query
     * @return This method returns the entry at the specified index
     */
    Entry getEntry(int index) {
    	// create queury using time as unique key, find and return entry
        SQLiteDatabase db = this.getReadableDatabase();
       
        	Cursor cursor = db.query(TABLE_ENTRIES, new String[] { KEY_INDEX, KEY_TIME,
                    KEY_GLUCOSE_LEVEL, KEY_PRE_MEAL }, KEY_INDEX + "=?",
                    new String[] { String.valueOf(index) }, null, null, null, null);
        	if (cursor != null)
                cursor.moveToFirst();

        Time timeTemp = stringToTime(cursor.getString(1));        
        double levelTemp = Double.parseDouble(cursor.getString(2));
        Boolean preTemp = Boolean.parseBoolean(cursor.getString(3));
        Entry entry = new Entry(timeTemp, levelTemp, preTemp);
        return entry;
    }
 
    /**
     * This method returns all Entries in table
     * 
     * @return - (List<Entry>) returns all entries in table
     */
    public List<Entry> getAllEntries() {
    	// iterate of all rows, save each entry in list, return list
        List<Entry> entryList = new ArrayList<Entry>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ENTRIES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // iterate over all rows in table
        if (cursor.moveToFirst()) {
            do {
                Time timeTemp = stringToTime(cursor.getString(1));
                double levelTemp = Double.parseDouble(cursor.getString(2));
                Boolean preTemp = Boolean.parseBoolean(cursor.getString(3));
                Entry tempEntry = new Entry(timeTemp, levelTemp, preTemp);
                entryList.add(tempEntry);
            } while (cursor.moveToNext());
        }
        return entryList;
    }
 
    /**
     * This method returns all Entries in table below specified value (EXCLUSIVE)
     * 
     * @return - (List<Entry>) returns all entries in table below specified val
     */
    public List<Entry> getEntriesUnder(double min) {
    	// TODO - currently returns all then selects
    	// iterate of all rows, save each entry in list, return list
        List<Entry> entryList = new ArrayList<Entry>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ENTRIES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // iterate over all rows in table
        if (cursor.moveToFirst()) {
            do {
            	Time timeTemp = stringToTime(cursor.getString(1));
                double levelTemp = Double.parseDouble(cursor.getString(2));
                Boolean preTemp = Boolean.parseBoolean(cursor.getString(3));
                Entry tempEntry = new Entry(timeTemp, levelTemp, preTemp);
                if(levelTemp < min){
                	 entryList.add(tempEntry);
                }
            } while (cursor.moveToNext());
        }
        return entryList;
    }
    
    /**
     * This method returns all Entries in table above specified value (EXCLUSIVE)
     * 
     * @return - (List<Entry>) returns all entries in table above specified val
     */
    public List<Entry> getEntriesOver(double max) {
    	// TODO - currently returns all then selects
    	// iterate of all rows, save each entry in list, return list
        List<Entry> entryList = new ArrayList<Entry>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ENTRIES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // iterate over all rows in table
        if (cursor.moveToFirst()) {
            do {
            	Time timeTemp = stringToTime(cursor.getString(1));
                double levelTemp = Double.parseDouble(cursor.getString(2));
                Boolean preTemp = Boolean.parseBoolean(cursor.getString(3));
                Entry tempEntry = new Entry(timeTemp, levelTemp, preTemp);
                if(levelTemp > max){
                	 entryList.add(tempEntry);
                }
            } while (cursor.moveToNext());
        }
        return entryList;
    }
    
    /**
     * This method returns all Entries in table within specified values (EXCLUSIVE)
     * 
     * @return - (List<Entry>) returns all entries in table within specified vals.
     */
    public List<Entry> getEntriesBetween(double min, double max) {
    	// TODO - currently returns all then selects
    	// iterate of all rows, save each entry in list, return list
        List<Entry> entryList = new ArrayList<Entry>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ENTRIES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // iterate over all rows in table
        if (cursor.moveToFirst()) {
            do {
            	Time timeTemp = stringToTime(cursor.getString(1));
                double levelTemp = Double.parseDouble(cursor.getString(2));
                Boolean preTemp = Boolean.parseBoolean(cursor.getString(3));
                Entry tempEntry = new Entry(timeTemp, levelTemp, preTemp);
                if(levelTemp > min && levelTemp < max){
                	 entryList.add(tempEntry);
                }
            } while (cursor.moveToNext());
        }
        return entryList;
    }
    
    /**
     * This method returns all Entries in table outside of specified values (EXCLUSIVE)
     * 
     * @return - (List<Entry>) returns all entries in table outside of specified vals.
     */
    public List<Entry> getEntriesOutside(double min, double max) {
    	// TODO - currently returns all then selects
    	// iterate of all rows, save each entry in list, return list
        List<Entry> entryList = new ArrayList<Entry>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ENTRIES;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
 
        // iterate over all rows in table
        if (cursor.moveToFirst()) {
            do {
            	Time timeTemp = stringToTime(cursor.getString(1));
                double levelTemp = Double.parseDouble(cursor.getString(2));
                Boolean preTemp = Boolean.parseBoolean(cursor.getString(3));
                Entry tempEntry = new Entry(timeTemp, levelTemp, preTemp);
                if(levelTemp > max || levelTemp < min){
                	 entryList.add(tempEntry);
                }
            } while (cursor.moveToNext());
        }
        return entryList;
    }
    
    /**
     * This method updated the Entry in the table with the unique time key.
     * 
     * @param entry - (Entry) The object to be updated, using time as key
     * @return - (boolean) Returns true if at least one row is updated
     */
    public boolean updateEntry(Entry entry) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_GLUCOSE_LEVEL, String.valueOf(entry.getGlucoseLevel()));
        values.put(KEY_PRE_MEAL, entry.getPreMeal().toString());
        int rowsUpdated;
        rowsUpdated = db.update(TABLE_ENTRIES, values, KEY_TIME + " = ?",
                new String[] { timeToString(entry.getTime()) });
        if(rowsUpdated > 0){
        	return true;
        }
        return false;
    }
 
    /**
     * This method deletes the specified entry, specified by time key.
     * 
     * @param entry - (Entry) Entry to delete from table
     * @return - (boolean) Returns true if at least one row is deleted
     */
    public boolean deleteEntry(Entry entry) {
        SQLiteDatabase db = this.getWritableDatabase();
        int rowsDeleted;
        rowsDeleted = db.delete(TABLE_ENTRIES, KEY_TIME + " = ?",
                new String[] { timeToString(entry.getTime()) });
        db.close();
        if(rowsDeleted > 0){
        	return true;
        }
        return false;
    }
 
    /**
     * This method deletes all entries in the database.
     */
    public void deleteAllEntries(){
    	SQLiteDatabase db = this.getWritableDatabase();
    	db.delete(TABLE_ENTRIES, null, null);
    	db.close();
    }
    
    /**
     * This method counts and returns the number of entries in table
     * @return - (int) returns the number of rows in table
     */
    public int getEntryCount() {
    	SQLiteDatabase db = this.getReadableDatabase();
        String countQuery = "SELECT  * FROM " + TABLE_ENTRIES;
        Cursor cursor = db.rawQuery(countQuery, null);
        int temp = cursor.getCount();
        db.close();
        return temp;
    }
    
    /**
     * This method creates a CSV file with the entries since the specified time
     * @param time - null if all entries should be in CSV file, else the time
     * 				 to display entries since (exclusive)
     * @return the file location of the CSV
     */
    public String toCSVFile(Time time){
    	String fileName = "log.csv"; 
    	
    	List<Entry> list = getAllEntries();
    	if(time != null){
    		// if only want entries after specified date
    		List<Entry> temp = new ArrayList<Entry>();
    		Iterator<Entry> iter = list.iterator();
    		while(iter.hasNext()){
    			Entry tempEntry = iter.next();
    			if(Time.compare(tempEntry.getTime(), time) > 0){
    				temp.add(tempEntry);
    			}
    		}
    		list.clear();
    		list = temp;
    	}
    	
    	try{
    		String filePath = Environment.getExternalStorageDirectory().getPath();
    		
    		File myFile = new File(filePath, fileName);
    		
    		if(myFile.exists()){
    		    myFile.delete();
    		}
    		
    		FileWriter write = new FileWriter(myFile, true);

    		SharedPreferences myPref = 
					PreferenceManager.getDefaultSharedPreferences(context);

    		String unitPref = myPref.getString("pref_glucose_units", 
					"0");
    		
			if(unitPref.equals("0")){
				unitPref = "mg/dL";
			}
			else{
				unitPref = "mmol/L";
			}
    		write.append("Time,Blood Glucose Level (" + unitPref + ") ,Pre Meal?" + "\n");
    		Iterator<Entry> iter = list.iterator();
    		while(iter.hasNext()){
    			Entry temp = iter.next();
    			write.append(timeToString(temp.getTime()) + "," + 
    					temp.getGlucoseLevel() + "," +
    					temp.getPreMeal().toString() +"\n");
    		}
    		write.close();
		}catch(Exception e){
			Log.d("Exception", "fail: toCSVFile");
		}
		
    	return fileName;
    }
}