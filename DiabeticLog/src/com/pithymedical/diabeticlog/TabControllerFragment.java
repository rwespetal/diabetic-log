package com.pithymedical.diabeticlog;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class TabControllerFragment extends Fragment {

	private static final int TABLE_STATE = 1;
	private static final int GRAPH_STATE = 2;

	private int tabState;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View view = inflater.inflate(R.layout.fragment_tab, container, false);

		// Grab the tab buttons from the layout and attach event handlers. The code just uses standard
		// buttons for the tab widgets. These are bad tab widgets, design something better, this is just
		// to keep the code simple.
		Button tableViewTab = (Button) view.findViewById(R.id.table_view_tab);
		Button graphViewTab = (Button) view.findViewById(R.id.graph_view_tab);

		tableViewTab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Switch the tab content to display the table view.
				gotoTableView();
			}
		});

		graphViewTab.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// Switch the tab content to display the graph view.
				gotoGraphView();
			}
		});

		return view;
	}


	public void gotoTableView(){
		// tabState keeps track of which tab is currently displaying its contents.
		// Perform a check to make sure the table tab content isn't already displaying.

		if (tabState != TABLE_STATE) {
			// Update the tabState 
			tabState = TABLE_STATE;

			// Fragments have access to their parent Activity's FragmentManager. You can
			// obtain the FragmentManager like this.
			FragmentManager fm = getFragmentManager();

			if (fm != null) {
				// Perform the FragmentTransaction to load in the list tab content.
				// Using FragmentTransaction#replace will destroy any Fragments
				// currently inside R.id.fragment_content and add the new Fragment
				// in its place.
				FragmentTransaction ft = fm.beginTransaction();
				ft.replace(R.id.fragment_content, new HistoryTableFragment());
				ft.commit();
			}
		}
	}


	public void gotoGraphView() {
		// See gotoListView(). This method does the same thing except it loads
		// the grid tab.

		if (tabState != GRAPH_STATE) {
			tabState = GRAPH_STATE;

			FragmentManager fm = getFragmentManager();

			if (fm != null) {
				FragmentTransaction ft = fm.beginTransaction();
				ft.replace(R.id.fragment_content, new HistoryGraphFragment());
				ft.commit();
			}
		}
	}

}
