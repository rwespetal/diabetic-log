package com.pithymedical.diabeticlog;

import java.io.File;

import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.*;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
		setContentView(R.layout.activity_main);
		listenForButtons();
	}

	public void listenForButtons (){
		Button entry = (Button) findViewById(R.id.entry);
		entry.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View entry) {
				Intent intent = new Intent(MainActivity.this, GlucoseEntry.class);
				startActivity(intent);
			}
		});
		Button history = (Button) findViewById(R.id.history);
		history.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View history) {
				Intent intent = new Intent(MainActivity.this, FragmentTabActivity.class);
				startActivity(intent);
			}
		});
		Button email = (Button) findViewById(R.id.email);
		email.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View email) {
				sendEmail(); 
			}
		});
		//TODO: Insulin calculator needs to be implemented
		Button calculator = (Button) findViewById(R.id.calculator);
		calculator.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View calculator) {
				Intent intent = new Intent(MainActivity.this, InsulinCalculator.class);
				startActivity(intent);
			}
		});
		Button settings = (Button) findViewById(R.id.settings);
		settings.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View settings) {
				startActivity(new Intent(MainActivity.this, SettingsActivity.class));
			}
		});
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	private void sendEmail(){
		//Get preferences and needed information
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		String provider_email = sharedPref.getString(SettingsActivity.ProviderEmail, "");
		String patient_name = sharedPref.getString(SettingsActivity.Name, ""); 
		String date_of_birth = sharedPref.getString(SettingsActivity.DOB, ""); 
		String insulin_ratio = sharedPref.getString(SettingsActivity.InsulinRatio, ""); 
		
		//Create intent for email
		Intent i = new Intent(Intent.ACTION_SEND);
		i.setType("message/rfc822");
		i.putExtra(Intent.EXTRA_EMAIL  , new String[]{provider_email});
		i.putExtra(Intent.EXTRA_SUBJECT, "Diabetic Log Records for Patient " + patient_name);
		
		String body = "Patient name: " + patient_name + "\n" +
				"Date of Birth: " + date_of_birth + "\n" +
				"Insulin Ratio: " + insulin_ratio;
		i.putExtra(Intent.EXTRA_TEXT, body);
		
		//Getting the data from the database
		DatabaseHandler db = new DatabaseHandler(getApplicationContext());
		String fileName = db.toCSVFile(null);
		
		File sd = new File(Environment.getExternalStorageDirectory().getPath(), fileName);
		Uri uri = Uri.parse("file://" + sd);
		i.putExtra(Intent.EXTRA_STREAM, uri); // + Environment.getExternalStorageDirectory() + attachment));
		
		//Try catch block in case they don't have a client installed
		try {
		    startActivity(Intent.createChooser(i, "Send mail..."));
		} catch (android.content.ActivityNotFoundException ex) {
		    Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
		}
	}
}
