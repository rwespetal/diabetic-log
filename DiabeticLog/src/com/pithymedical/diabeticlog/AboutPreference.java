package com.pithymedical.diabeticlog;


import android.content.Context;
import android.preference.DialogPreference;
import android.util.AttributeSet;

public class AboutPreference extends DialogPreference {
    public AboutPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
        	setDialogLayoutResource(R.layout.about_preference);
        	setPositiveButtonText(android.R.string.ok);
        	
        	//Removes the cancel button, just displays the ok
        	setNegativeButtonText("");
        	setDialogIcon(null);
    }
}
