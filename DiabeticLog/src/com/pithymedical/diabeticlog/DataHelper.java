package com.pithymedical.diabeticlog;

import java.util.ArrayList;
import java.util.List;

import android.text.format.Time;

public class DataHelper {



        public static ArrayList<Time> getTimes(List<Entry> entries){

                ArrayList<Time> times = new ArrayList<Time>();
                
                if(entries == null){
                        return times;
                }

                for(int i = 0; i < entries.size(); i++){
                        times.add(entries.get(i).getTime());
                }

                return times;

        }
        
        public static ArrayList<Long> getTimesLong(List<Entry> entries){

                ArrayList<Long> times = new ArrayList<Long>();
                
                if(entries == null){
                        return times;
                }

                for(int i = 0; i < entries.size(); i++){
                        times.add(entries.get(i).getTime().toMillis(true));
                }

                return times;

        }

        
        
        public static ArrayList<Double> getGlucoseLevels(List<Entry> entries){

                ArrayList<Double> levels = new ArrayList<Double>();

                if(entries == null){
                        return levels;
                }
                
                for(int i = 0; i < entries.size(); i++){
                        levels.add(entries.get(i).getGlucoseLevel());
                }

                return levels;

        }
        
        public static ArrayList<Double> getDateDouble(ArrayList<Time> times){
        	ArrayList<Double> datesFormatted = new ArrayList<Double>();
        	
        	for(int i = 0; i < times.size(); i++){
        		String newDate = getNewDate(DatabaseHandler.timeToString(times.get(i)));
        		datesFormatted.add(Double.parseDouble(newDate));
        	}
        	
        	
        	
        	return datesFormatted;
        }
        
        public static String getNewDate(String time){
        	
    		String[] split = time.split(" ");

    		String date = split[0];
    		time = split[1];

    		date = date.substring(date.indexOf('-')+1);
    		date.replaceFirst("-", ".");
        	
        	return date;
        }
        
        

}