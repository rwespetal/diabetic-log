package com.pithymedical.diabeticlog;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

import android.text.format.Time;

public class DateTimeFormatter extends Format{

	private static final long serialVersionUID = 1L;

	@Override
	public StringBuffer format(Object time, StringBuffer mBuff,
			FieldPosition pos) {
		mBuff = new StringBuffer();
				
		double dTime = (Double) time;
		
		Time newTime = new Time();
		newTime.set((long) dTime);
		
		String date = DataHelper.getNewDate(DatabaseHandler.timeToString(newTime));
		String[] dateSplit = date.split("-");
		mBuff.append(dateSplit[0] + "/" + dateSplit[1]);
		
		return mBuff;
	}

	@Override
	public Object parseObject(String arg0, ParsePosition arg1) {
		// TODO Auto-generated method stub
		return null;
	}

}
