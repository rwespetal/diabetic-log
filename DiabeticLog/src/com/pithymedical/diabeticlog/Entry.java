package com.pithymedical.diabeticlog;

import java.util.*;
import android.text.format.Time;

public class Entry {
	
	// private variables
    Time time = null;
    double glucoseLevel = -1;
    Boolean preMeal = null;
 
    /**
     * @param time - (Time) time to record
     * @param glucoseLevel - (double) glucose level to record in mg/dL
     * @param preMeal - (boolean) whether measurement was pre-meal
     * 
     * This method creates an Entry instance with the specified params
     */
    public Entry(Time time, double glucoseLevel, Boolean preMeal){
    	if(glucoseLevel < 0 || preMeal == null || time == null){
    		throw new InputMismatchException();
    	}
        this.time = time;
        this.glucoseLevel = glucoseLevel;
        this.preMeal = preMeal;
    }
 
    /**
     * @param glucoseLevel - (double) glucose level to record in mg/dL
     * @param preMeal - (boolean) whether measurement was pre-meal
     * 
     * This method creates a new Entry instance with specified params and
     * current time. 
     */
    public Entry(double glucoseLevel, Boolean preMeal){
    	if(glucoseLevel < 0 || preMeal == null){
    		throw new InputMismatchException();
    	}
        this.glucoseLevel = glucoseLevel;
        this.preMeal = preMeal;
        Time temp = new Time();
        temp.setToNow();
        this.time = temp;
    }
    
    /**
     * @return (double) glucose Level on Entry instance.
     */
    public double getGlucoseLevel(){
        return this.glucoseLevel;
    }
 
    /**
     * @param level - (double) glucose level to set for Entry instance.
     */
    public void setGlucoseLevel(double level){
    	if(level < 0){
    		throw new InputMismatchException();
    	}
    	this.glucoseLevel = level;
    }
 
    /**
     * @return - (Boolean) preMeal Boolean value
     */
    public Boolean getPreMeal(){
        return this.preMeal;
    }
 
    /**
     * @param preMeal - (boolean) Sets Entry preMeal property to specified value
     */
    public void setPreMeal(Boolean preMeal){
        if (preMeal == null){
        	throw new InputMismatchException();
        }
        this.preMeal = preMeal;
    }
 
    /**
     * @return - (Time) time property of Entry instance
     */
    public Time getTime(){
        return this.time;
    }
 
    /**
     * @param time - (Time) Sets Entry time property to specified value
     */
    public void setTime(Time time){
        if(time == null){
        	throw new InputMismatchException();
        }
        this.time = time;
    }
    
    
    /**
     * @return - printable version of an entry
     */
    @Override
    public String toString(){
    	return time + ": " + glucoseLevel  + ", " + preMeal;
    }
}