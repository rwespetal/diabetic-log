package com.pithymedical.diabeticlog;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.Window;


public class SettingsActivity extends PreferenceActivity {
	public static final String ProviderEmail = "provider_email";
	public static final String Name = "pref_name";
	public static final String DOB = "dob";
	public static final String InsulinRatio = "insulin_ratio";
	public static final String notify = "pref_notifications";

	public static final int MMOL_LITER = 0;
	public static final int MG_DL = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//Remove title bar
		requestWindowFeature(Window.FEATURE_NO_TITLE); 
    	super.onCreate(savedInstanceState);
		if(Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB){
			createPreferenceActivity(); 
		}
		else{
			createPreferenceFragment(); 
		}	
	}
	@SuppressLint("NewApi")
	private void createPreferenceFragment(){
		// Display the fragment as the main content.
		getFragmentManager().beginTransaction()
		.replace(android.R.id.content, new SettingsFragment())
		.commit();
	}
	//TODO: shared prefs? 
	@SuppressWarnings("deprecation")
	private void createPreferenceActivity(){
        addPreferencesFromResource(R.layout.preferences);
	}
}
